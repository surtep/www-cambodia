<div class="row">

@if(! Auth::user())
<div class="col-md-12">
    <strong><a href="/auth/login">Login</a></strong>
    <!-- &nbsp;&nbsp;
    <strong>Support: </strong>+90-897-678-44 -->
</div>
@endif

@if(Auth::user())
    <div class="col-md-12">
        Bonjour <strong>{{ Auth::user()->name }} </strong> &nbsp;&nbsp; {{ Auth::user()->email }}
        &nbsp;&nbsp;
        <strong><a href="/auth/logout">Logout</a></strong>
    </div>
</div>
@endif