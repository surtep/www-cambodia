<section class="menu-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
                    <li><a  href="/backend" class="@if(Request::segment(1)=='backend' && !Request::segment(2)) menu-top-active @endif">Home</a></li>
                        <li><a  href="{{ route('backend.event.index') }}" class="@if(Request::segment(2)=='event') menu-top-active @endif">Events</a></li>
                        <li><a href="/backend/users" class="@if(Request::segment(2)=='users') menu-top-active @endif">Users</a></li>
                        <li><a href="/backend/filemanager" class="@if(Request::segment(2)=='filemanager') menu-top-active @endif">FileManager</a></li>
                        <li><a href="/backend/settings" class="@if(Request::segment(2)=='setting') menu-top-active @endif">Settings</a></li>
                        <li><a href="/backend/categories" class="@if(Request::segment(2)=='categories') menu-top-active @endif">Categories</a></li>
                        <!-- <li><a href="forms.html">Forms</a></li>
                         <li><a href="login.html">Login Page</a></li>
                        <li><a class="menu-top-active" href="blank.html">Blank Page</a></li> -->

                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>