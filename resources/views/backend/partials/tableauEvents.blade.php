<div style="margin-top:50px">
<div class="pull-right">
{!! $events->render() !!}
</div>
<table class="table table-striped">
<tr>
	<th>Status</th>
	<th>Pos</th>
	<th>Title</th>
	<th>Where</th>
	<th>When</th>
	<th>Published at </th>
	<th>What</th>
	<th>Actions</th>
</tr>
@foreach ($events as $event)

	<tr class="{{ $event->eventWhen < \Carbon\Carbon::now() ? 'offlineEvent' : '' }} ">
		<td class="status{{ $event->approved }}"><i class=" status fa @if($event->approved==1) {{ "fa-check" }} @else {{ "fa-times" }}@endif "></i>
		<td>{{ $event->position }}</td>
		<td>{{ str_limit($event->title_en, 20) }}</td>
		<td>{{ str_limit($event->eventWhere, 20) }}</td>
		<td>{{ $event->eventWhen }} <br> ({{ $event->eventWhen->diffForHumans() }})</td>
		<td><span class="@if( \Carbon\Carbon::now() > $event->published_at ) {{'active'}} @else {{'inactive'}} @endif">{{ $event->published_at->diffForHumans() }}</span></td>
		<td>{{ $event->what->title_en }}</td>
		<td><a href="{{ route('backend.events.edit', $event->id) }}" class="btn btn-xs btn-primary btn-generique">Edit</a> &nbsp; 
		<a href="#" class="btn btn-danger btn-xs btn-generique" data-toggle="modal" data-target="#deleteModal" 
		data-optionsmodal = "{{ $event->id }} | {{ trans('backend.deleteEvent') }} | {{ trans('backend.reallyDeleteEvent', ['nomEvent' => $event->title_en]) }} | events" 
		>Delete</a>
		</td>
	</tr>
@endforeach
</table>
<div class="pull-right">
	{!! $events->render() !!}
</div>
</div>