<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="deleteModalLabel">Effacer VRAIMENT cette fiche ?</h4>
            </div>
            <div class="modal-body" id="deleteModalBody">
                Texte qui sera remplacé
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left btn-generique" data-dismiss="modal" >Cancel</button>
                {!! Form::open([ 'method' => 'DELETE', 'route' => ['backend.events.destroy', '2'], "id" => "btn-destroy" ]) !!} {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-generique']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>