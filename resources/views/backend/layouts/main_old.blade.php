<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>{{ config('www-cambodia.title') }}</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="/assets/backend/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="/assets/backend/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="/assets/backend/css/style.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('topscripts')
</head>
<body>
    <header>
        <div class="container">
            @include('backend.partials.topMenuBar')
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/backend">
                    <img class="logoBackend" src="/assets/backend/img/logowww-petit.jpg" alt="Home backend"/>
                </a>
                <div class="pull-right similiH1">
                {{ config('www-cambodia.title') }} - Administration
                </div>
            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    @if(Auth::user())
    @include('backend.partials.menuPrincipal')
    @endif
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
        @include('flash::message')
        @include('backend.partials.errors')
    @yield('content')
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        @include('backend.partials.footer')
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="/assets/backend/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="/assets/backend/js/bootstrap.js"></script>
    @yield('bottomscripts')
</body>
</html>