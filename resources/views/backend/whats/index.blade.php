@extends('backend.layouts.main') @section('content')

<div class="row">
    <div class="col col-lg-6">
    <h1>Liste des Whats</h1>
    </div>
    {{-- <div class="col col-lg-6">
    <a href="{{ Route('backend.whats.create') }}" class="btn btn-lg btn-primary pull-right">Add new category</a>
    </div> --}}
</div>

<table class="table table-striped">
    <tr>
        <th>Title</th>
        <th>KH</th>
        <th>FR</th>
        <th>Icon</th>
        <th>Actions</th>
    </tr>
    @foreach ($whats as $what)
    <tr>
        <td>{{ $what->title_en }}</td>
        <td>{{ $what->title_kh }}</td>
        <td>{{ $what->title_fr }}</td>
        <td><i class="fa fa-fw {{ $what->icon }}"></i></td>
        <td><a href="{{ route('backend.whats.edit', $what->id) }}" class="btn btn-xs btn-primary btn-generique">Edit</a>
        <a href="#" class="btn btn-danger btn-xs btn-generique" data-toggle="modal" data-target="#deleteModal" 
        data-optionsmodal = "{{ $what->id }} | Delete this Categorie ? | {{ $what->title_en }} | whats" 
        >Delete</a>
        </td>
    </tr>
    @endforeach

</table>

@include('backend.partials.modal')

@endsection

@section('bottomscripts')
<script src = "/assets/backend/js/www-cambodia.js"></script>
@endsection
