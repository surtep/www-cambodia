@extends('backend.layouts.main')

@section('content')

{!! Form::model($what,[
'route'=>['backend.whats.update', $what->id],
'method' => 'PATCH'
]) !!}
@include('backend.whats.form')

{!! Form::close() !!}	


@endsection