<h1 class="">{!! $what->title_en or 'Creating a new Category' !!}</h1>


<div class="row">
	<div class="col col-lg-9">
		<div class="form-group">
		    {!! Form::label('title_en', 'Title:', ['class' => 'control-label']) !!}
		    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
		    {!! Form::label('title_kh', 'Title KH:', ['class' => 'control-label']) !!}
		    {!! Form::text('title_kh', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
		    {!! Form::label('title_fr', 'Titre FR:', ['class' => 'control-label']) !!}
		    {!! Form::text('title_fr', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
		    {!! Form::label('icon', 'Icone:', ['class' => 'control-label']) !!}
		    {!! Form::text('icon', null, ['class' => 'form-control']) !!}
		</div>

	</div>
	<div class="col col-md-3">
{!! Form::submit('Enregistrer', ['class' => "btn btn-success btn-sm pull-right btn-savepage"]) !!}
	</div>
</div>
