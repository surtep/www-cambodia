@extends('backend.layouts.main')

@section('content')

{!! Form::open(['route'=>'backend.events.store', 'class'=> '']) !!}	

@include('backend.events.form')

{!! Form::close() !!}	


@endsection

@section('topscripts')
<link rel="stylesheet" href="{{ asset('assets/backend/css/jquery.simple-dtpicker.css') }}">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="http://cdn.roxyfileman.com/fancybox/source/jquery.fancybox.min.css" rel="stylesheet">
<script defer="" language="javascript" type="text/javascript" src="http://cdn.roxyfileman.com/js/main.js"></script>


<script defer="" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script defer="" language="javascript" type="text/javascript" src="http://cdn.roxyfileman.com/js/main.js"></script>
<script language="javascript" type="text/javascript">

function addFancyBox(){
  if($) {
    try{
      $('a[rel^="lightbox"]').fancybox({prevEffect:'fade',nextEffect:'fade'});
    }
    catch(exc){
      //setTimeout('addFancyBox()', 500);
    }
  }
  else{
    setTimeout('addFancyBox()', 500); 
  }
}
function jqueryLoaded(){
	$('li.selected').parents('li').attr('class','selected');
};

</script>
<script defer="" onload="addFancyBox()" type="text/javascript" src="http://cdn.roxyfileman.com/fancybox/source/jquery.fancybox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://google-code-prettify.googlecode.com/svn/loader/prettify.css"><style type="text/css">.fancybox-margin{margin-right:0px;}</style>



@endsection

@section('bottomscripts')
{{-- <script type="text/javascript" src="{{ asset('assets/backend/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/backend/ckeditor/adapters/jquery.js') }}"></script>
<script src="/assets/backend/ckeditor/maConfig.js"></script> --}}

<script src="{{ asset('assets/backend/js/jquery.simple-dtpicker.js') }}"></script>
<script type="text/javascript">
	$(function(){

		$('*[name=eventWhen]').appendDtpicker({'closeOnSelected':true});
		$('*[name=published_at]').appendDtpicker({'closeOnSelected':true});
	});

</script>
{{-- <script type="text/javascript" src="{{ asset('assets/backend/ckeditor/ckeditor.js') }}"></script> --}}
<script>
var importFiles = new Array('http://cdn.roxyfileman.com/fancybox/source/jquery.fancybox.min.css', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
var cb = function() {
  for(i in importFiles) {
    var l = document.createElement('link'); 
    l.rel = 'stylesheet';
    l.href = importFiles[i];
    var h = document.getElementsByTagName('head')[0]; 
    h.parentNode.insertBefore(l, h);
  }
};
var raf = requestAnimationFrame || mozRequestAnimationFrame ||
    webkitRequestAnimationFrame || msRequestAnimationFrame;
if (raf) raf(cb);
else window.addEventListener('load', cb);
</script>
<script>
function openCustomRoxy2(){
  $('#roxyCustomPanel2').dialog({modal:true, width:875,height:600});
}
function closeCustomRoxy2(){
  $('#roxyCustomPanel2').dialog('close');
}
</script>

<script>
$(function () {
  $('input[name=when_end]').focus(function(){
    if($(this).val()==''){
      var minAnciennes = $('input[name=eventWhen]').val().substr(14, 2);
      var heureNouvelle = parseInt($('input[name=eventWhen]').val().substr(11, 2)) + 1;
      // alert(valWhen);
      heureDefault = heureNouvelle +':'+minAnciennes;
      $(this).val(heureDefault);
    }
  });
});
</script>

@endsection