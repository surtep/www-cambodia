@extends('backend.layouts.main')

@section('content')

<div class="row">
	<div class="col col-lg-6">
	<h1>Events in <span class="rouge"> {{ session('cityActive') }} </span></h1>
	</div>
	<div class="col col-lg-6">
	<a href="{{ Route('backend.events.create') }}" class="btn btn-lg btn-primary pull-right btn-generique">Add new event</a>
	</div>
	
</div>

<div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="tab" href="#today-pills">Today</a>
                                </li>
                                <li><a data-toggle="tab" href="#tomorrow-pills">Tomorrow</a>
                                </li>
                                <li><a data-toggle="tab" href="#next-pills">Next</a>
                                </li>
                                <li><a href="/backend/calendar">Calendar</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="today-pills" class="tab-pane fade in active">
                                    @include('backend.partials.tableauEvents', ['events' => $todayEvents])
                                </div>
                                <div id="tomorrow-pills" class="tab-pane fade">
                                    @include('backend.partials.tableauEvents', ['events' => $tomorrowEvents])
                                </div>
                                <div id="next-pills" class="tab-pane fade">
                                   @include('backend.partials.tableauEvents', ['events' => $nextEvents])
                                </div>
                            </div>
                        </div>


@include('backend.partials.modal')

@endsection

@section('bottomscripts')
<script src = "/assets/backend/js/www-cambodia.js"></script>
<script>
$(function () {
    $(".status").click(function(){
        if($(this).hasClass('fa-check')) alert('fa-check');
    })
});
</script>
@endsection