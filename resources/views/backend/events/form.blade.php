@inject('categoriesSelect', 'App\Services\CreateSelectCategories')

<div class="row">
	<div class="col-md-12 col-sm-12">
	                    <div class="">
	                        <div class="panel-heading {{ $typePage or 'default' }}">
	                        <div class="row">
	                        <div class="col col-md-6">
	                         <h1 class="">{!! $event->title_en or 'Creating a new event' !!}</h1>
	                        </div>	
	                        	
	                        </div>

</div>
	                         <span class="form-group sr-only">
	                         
	                         </span>
			
	                        </div>
	                        <div class="panel-body">
	                            <ul class="nav nav-tabs">
	                                <li class="active"><a href="#page" data-toggle="tab">Event</a>
	                                </li>
	                                {{-- <li class=""><a href="#picture" data-toggle="tab">Picture</a>
	                                </li> --}}
	                                <li class=""><a href="#options" data-toggle="tab">Options</a>
	                                </li>
	                                <li class="pull-right">
	                                <div class="btn-group" role="group">
	                                <a href="{{ route('backend.events.index') }}" class="btn btn-default btn-sm btn-savepage">Cancel</a>
				{!! Form::submit('Enregistrer', ['class' => "btn btn-success btn-sm btn-savepage"]) !!}
	                                </div>
	                                </li>
	                                
	                            </ul>
	
	                            <div class="tab-content">
	                                <div class="tab-pane fade active in" id="page">
				<div class="row">
					<div class="col col-lg-8">
						<div class="form-group">
						    {!! Form::label('title_en', 'Title:', ['class' => 'control-label']) !!}
						    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-md-2">
	                        	                         <span class="form-group pull-right">
	                        	                         <?php if(isset($event)) {
	                        	                             $categorieActive = $event->what_id;
	                        	                             }  else {
	                        	                             $categorieActive = 1;
	                        	                                 };
	                        	                                 ?>
	                        	                             {!! Form::label('what_id', 'Category : ', ['class' => 'control-label']) !!}
	                        	                             {!! Form::select('what_id', App\What::lists('title_en', 'id'), $categorieActive, ['class' => 'form-control'] ) !!}
	                        	                         </span>
		        	                        </div>
					<div class="col col-md-2">
	                        	                         <span class="form-group pull-right">
	                        	                         <?php if(isset($event)) {
	                        	                             $cityActive = $event->city_id;
	                        	                             }  else {
	                        	                             $cityActive = 1;
	                        	                                 };
	                        	                                 ?>
	                        	                             {!! Form::label('city', 'City : ', ['class' => 'control-label']) !!}
	                        	                             {!! Form::select('city', App\City::lists('city', 'id'), $cityActive, ['class' => 'form-control'] ) !!}
	                        	                         </span>
		        	                        </div>
					
				</div>
				<div class="row">
					<div class="col col-md-3">
						<div class="form-group">
						    {!! Form::label('eventWhere', 'Where:', ['class' => 'control-label']) !!}
						    {!! Form::text('eventWhere', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-md-3">
						{!! Form::label('eventWhen', 'When', ['class' => 'control-label']) !!}
						{!! Form::text('eventWhen', null, ['class' => 'form-control']) !!}
					</div>
					<div class="col col-md-3">
						{!! Form::label('when_end', 'Time End', ['class' => 'control-label']) !!}
						{!! Form::text('when_end', null, ['class' => 'form-control']) !!}
					</div>
					<div class="col col-md-3">
						{!! Form::label('published_at', 'Publish date', ['class' => 'control-label']) !!}
						{!! Form::text('published_at', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				@if(Auth::user()->hasRole('superadmin'))
				<div class="row superAdmin">
					<div class="col col-md-4">
						{!! Form::label('approved', 'Approved', ['class' => 'control-label']) !!}
						{!! Form::checkbox('approved', null, null, ['class' => 'l']) !!}
						
					</div>
					<div class="col col-md-4">
						{!! Form::label('position', 'Position', ['class' => 'control-label']) !!}
						{!! Form::text('position', null, ['class' => 'form-control']) !!}
					</div>
					<div class="col col-md-4">
						{!! Form::label('likes', 'Likes', ['class' => 'control-label']) !!}
						{!! Form::text('likes', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				@endif

				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
						    {!! Form::label('description_en', 'Description:', ['class' => 'control-label']) !!}
						    {!! Form::textarea('description_en', null, ['class' => 'form-control ckeditor']) !!}
						</div>
					</div>
					
				</div>


<div class="row">
	<div class="col col-lg-6">
	{!! Form::label('txtSelectedFile', 'Select a Picture:', ['class' => 'control-label']) !!}
<input type="text" id="txtSelectedFile" name="pictureEvent" style="border:1px solid #ccc;cursor:pointer;padding:4px;width:80%;" value="{{ $event->pictureEvent or '' }}" onclick="openCustomRoxy2()">
<div id="roxyCustomPanel2" style="display: none;">
  <iframe src="/assets/backend/fileman/index.html?integration=custom&type=files&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0">
  </iframe>
				</div>
				</div>
  	<div class="col col-lg-6">
  		<img src="{{ $event->pictureEvent or 'No image' }}" alt="" width="120" height="100">
  	</div>
				
				</div>
  <div class="row">
  </div>

				<div class="row">
				<h3>Khmer</h3>
				<div class="col col-lg-8">
				<div class="form-group">
				    {!! Form::label('title_kh', 'Title:', ['class' => 'control-label']) !!}
				    {!! Form::text('title_kh', null, ['class' => 'form-control']) !!}
				</div>
				</div>
				</div>
				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
						    {!! Form::label('description_kh', 'Description:', ['class' => 'control-label']) !!}
						    {!! Form::textarea('description_kh', null, ['class' => 'form-control ckeditor']) !!}
						</div>
					</div>
					
				</div>

				<div class="row">
				<h3>Français</h3>
				<div class="col col-lg-8">
				<div class="form-group">
				    {!! Form::label('title_fr', 'Titre:', ['class' => 'control-label']) !!}
				    {!! Form::text('title_fr', null, ['class' => 'form-control']) !!}
				</div>
				</div>
				</div>
				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
						    {!! Form::label('description_fr', 'Description:', ['class' => 'control-label']) !!}
						    {!! Form::textarea('description_fr', null, ['class' => 'form-control ckeditor']) !!}
						</div>
					</div>
					
				</div>


				<div class="row">
				<div class="col-md-4">
				         <div class="form-group">
				            
				          </div>
				     </div>
			            </div>
	                                </div>


	                                {{-- <div class="tab-pane fade" id="picture">
				<div class="row">
<input type="text" id="txtSelectedFile" name="pictureEvent" style="border:1px solid #ccc;cursor:pointer;padding:4px;width:80%;" value="{{ $event->pictureEvent or 'Click here to select an image' }}" onclick="openCustomRoxy2()">
<div id="roxyCustomPanel2" style="display: none;">
  <iframe src="/assets/backend/fileman/index.html?integration=custom&type=files&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0">
  </iframe>
				</div>
				</div>
  <div class="row">
  	<div class="col col-lg-12">
  		<img src="{{ $event->pictureEvent or 'No image' }}" alt="">
  	</div>
  </div>
  </div> --}}

	                                <div class="tab-pane fade" id="options">
	                                    <div class="row">
	                                    	<div class="col col-lg-12">

					


	                                    	@if(isset($event))
	                                    	<table class="table table-striped">
	                                    		<tr>
	                                    			<th>Mention</th>
	                                    			<td>Explication</th>
	                                    		</tr>
	                                    	<tr>
	                                    		<th>Created by:</td>
	                                    		<td>{{ App\User::first()->name }}</td>
	                                    	</tr>
	                                    	<tr>
	                                    		<th>Created:</td>
	                                    		<td>{{ $event->created_at }}</td>
	                                    	</tr>
	                                    	</table>
                                    		@endif
	                                    	</div>
	                                    </div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
</div>