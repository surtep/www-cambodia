@inject('getEvents', 'App\Services\GetEventsCalendar')

@extends('backend.layouts.main')

@section('content')

<h1>Calendar view</h1>

{{-- {!! $getEvents->getEventsList() !!} --}}

<div id='calendar'></div>

@include('backend.partials.modal')

@endsection

@section('topscripts')
<link href='/assets/backend/css/fullcalendar.css' rel='stylesheet' />
<link href='/assets/backend/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<meta name="_token" content="{{ csrf_token() }}" />
<style>

    /*body {
   margin: 40px 10px;
        padding: 0;
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
    }    */

    #calendar {
        max-width: 100%;
        margin: 0 auto;
    }

</style>
@endsection

@section('bottomscripts')
<script src = "/assets/backend/js/www-cambodia.js"></script>
<script src='/assets/backend/js/moment.min.js'></script>
{{-- <script src='/assets/backend/js/jquery.min.js'></script> --}}
<script src='/assets/backend/js/fullcalendar.min.js'></script>
<script>

    $(document).ready(function() {
$.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } ;
    if(mm<10){
        mm='0'+mm
    } ;
    var today = yyyy+'-'+mm+'-'+dd;


        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: today,
            editable: true,
            eventLimit: false, // allow "more" link when too many events
           events:{!! $getEvents->getEventsList() !!},
           eventDrop: function(event, delta, revertFunc) {
                enregistrerChangeEvent(event);
                    },
           eventResize: function(event, delta, revertFunc) {
            // alert(event.title + " end is now " + event.end.format());
            enregistrerChangeEnd(event);
                },
                eventClick: function(calEvent, jsEvent, view) {
                    window.location(calEvent.url);
                },
                dayClick: function(date, jsEvent, view) {
                    window.location = '/backend/events/create?id=25';

        // alert('Clicked on: ' + date.format());

        // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

        // alert('Current view: ' + view.name);

        // // change the day's background color just for fun
        // $(this).css('background-color', 'red');
            }
         
        });
function enregistrerChangeEvent(event){
    // console.log("coucou");
// alert(event.id + " est déplacé au " + event.start.format() + " End: "+ event.end.format()); 
var dataString = "id=" + event.id + "&newDate="+ event.start.format() + "&newHeure="+ event.end.format();
$.ajax({
            type: "GET",
            url: "/backend/api/enregistrerChangeEvent",
            data: dataString,
            dataType: "html",
            success: function(data) {
                // alert(data);
            }
        }); 
    };

function enregistrerChangeEnd(event){
// alert(event.id + " est déplacé au " + event.start.format()); 
var dataString = "id=" + event.id + "&newHeure="+ event.end.format();
$.ajax({
            type: "GET",
            url: "/backend/api/enregistrerChangeEnd",
            data: dataString,
            dataType: "html",
            success: function(data) {
                // alert(data);
            }
        }); 
    };

});

</script>
@endsection