
<div class="row">
	<div class="col-md-12 col-sm-12">
	                    <div class="">
	                        <div class="panel-heading {{ $typePage or 'default' }}">
	                        <div class="row">
	                        <div class="col col-md-6">
	                         <h1 class="">{!! $user->name or 'Creating a new User' !!}</h1>
	                        </div>	
	                        	
	                        </div>
	                         <span class="form-group sr-only">
	                         
	                         </span>
			
	                        </div>
	                        <div class="panel-body">
	                            <ul class="nav nav-tabs">
	                                <li class="active"><a href="#page" data-toggle="tab">User</a>
	                                </li>
	                                <li class=""><a href="#options" data-toggle="tab">Options</a>
	                                </li>
	                                <li class="pull-right">
	                                <div class="btn-group" role="group">
	                                <a href="{{ route('backend.users.index') }}" class="btn btn-default btn-sm btn-savepage">Cancel</a>
				{!! Form::submit('Enregistrer', ['class' => "btn btn-success btn-sm btn-savepage"]) !!}
	                                </div>
	                                </li>
	                                
	                            </ul>
	
	                            <div class="tab-content">
	                                <div class="tab-pane fade active in" id="page">
				<div class="row">
					<div class="col col-lg-3">
						<div class="form-group">
						    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
						    {!! Form::text('name', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-lg-3">
						<div class="form-group">
						    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
						    {!! Form::text('email', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-lg-3">
						<div class="form-group">
						    {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
						@if(isset($user))
						@if(Auth::user()->hasRole('superadmin'))
						<a href="{{ route('changePassword', ['id' => $user->id])}}" class="btn btn-warning ">Click here to edit password</a>	
						@else
						<span class="btn btn-danger">You can not change the password</span>
						@endif
						@else
						    {!! Form::text('password', null, ['class' => 'form-control']) !!}
						    @endif
						</div>
					</div>
					<div class="col col-lg-3">
	                        	                         <span class="form-group pull-right">
	                        	                         {!! Form::label('role', 'Role : ', ['class' => 'control-label']) !!}
	                        	                          {!! Form::select('role', Bican\Roles\Models\Role::lists('name', 'id'), 
					isset($user) ? $user->roles[0]['id'] : 1,
					['class' => 'form-control' ,  Auth::user()->is('superadmin') ? '' : 'disabled'  ] ) !!}
	                        	                         </span>
		        	                        </div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<div class="form-group">
						</div>
					</div>
					<div class="col col-md-3">
					</div>
					<div class="col col-md-3">
					</div>
				</div>
				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
						</div>
					</div>
				</div>
				<div class="row">
				<div class="col-md-4">
				         <div class="form-group">
				          </div>
				     </div>
			            </div>
	                                </div>

	                                <div class="tab-pane fade" id="options">
	                                    <div class="row">
	                                    	<div class="col col-lg-12">
                                    		<p>User's Options</p>
	                                    	</div>
	                                    </div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
</div>