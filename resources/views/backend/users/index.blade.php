@extends('backend.layouts.main')

@section('content')

<div class="row">
	<div class="col col-lg-6">
	<h1>Liste des Users</h1>
	</div>
	<div class="col col-lg-6">
	<a href="{{ Route('backend.users.create') }}" class="btn btn-lg btn-primary pull-right btn-generique">Add new user</a>
	</div>
</div>
<div class="pull-right">
	{!! $users->render() !!}
</div>
<table class="table table-striped">
<tr>
	<th>name</th>
	<th>email</th>
	<th>Registred from</th>
	<th>Role</th>
	<th>Actions</th>
</tr>
@foreach ($users as $user)

	<tr>
		<td>{{ $user->name }}</td>
		<td>{{ $user->email }}</td>
		<td>{{ $user->created_at }} <br> ({{ $user->created_at->diffForHumans() }})</td>
		<td>{{ $user->roles[0]['name'] }} </td>
		<td><a href="{{ route('backend.users.edit', $user->id) }}" class="btn btn-xs btn-primary btn-generique">Edit</a> &nbsp; 
		<a href="#" class="btn btn-danger btn-xs btn-generique" data-toggle="modal" data-target="#deleteModal" 
		data-optionsmodal = "{{ $user->id }} | Delete this User ? | {{ $user->name }} | users" 
		>Delete</a>
		</td>
	</tr>
@endforeach
</table>
<div class="pull-right">
	{!! $users->render() !!}
</div>
@include('backend.partials.modal')

@endsection

@section('bottomscripts')
<script src = "/assets/backend/js/www-cambodia.js"></script>
@endsection