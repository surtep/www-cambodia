@extends('backend.layouts.main')

@section('content')

{!! Form::model($user,[
'route'=>['backend.users.update',  $user->id],
'method'=> 'PATCH'
]) !!}	

@include('backend.users.form')

{!! Form::close() !!}	


@endsection

@section('topscripts')
@endsection

@section('bottomscripts')
@endsection