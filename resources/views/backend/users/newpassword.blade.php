@extends('backend.layouts.main')

@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">New password for {{ $user->name }} </h4>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
           <form class="form-horizontal" role="form" method="POST" action="{{ route('updatePassword') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <label for="password">Enter Password : </label>
                 <input type="hidden" value="{{ $user->id }}" name="id">
                    <input type="text" id="password" name="password" class="form-control" />
                    <label for="password_confirmation">Confirm Password :  </label>
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" />
                    <hr />
                    <button type="submit"><span class="glyphicon glyphicon-user"></span> &nbsp;Change Password </button>&nbsp;
             </form>
            </div>
            <div class="col-md-6">
                <div class="alert alert-info">
                    This is a free bootstrap admin template with basic pages you need to craft your project. 
                    Use this template for free to use for personal and commercial use.
                    <br />
                     <strong> Some of its features are given below :</strong>
                    <ul>
                        <li>
                            Responsive Design Framework Used
                        </li>
                        <li>
                            Easy to use and customize
                        </li>
                        <li>
                            Font awesome icons included
                        </li>
                        <li>
                            Clean and light code used.
                        </li>
                    </ul>
                   
                </div>
                <div class="alert alert-success">
                     <strong> Instructions To Use:</strong>
                    <ul>
                        <li>
                           Lorem ipsum dolor sit amet ipsum dolor sit ame
                        </li>
                        <li>
                             Aamet ipsum dolor sit ame
                        </li>
                        <li>
                           Lorem ipsum dolor sit amet ipsum dolor
                        </li>
                        <li>
                             Cpsum dolor sit ame
                        </li>
                    </ul>
                   
                </div>
            </div>

        </div>
    </div>
</div>
@endsection