@extends('backend.layouts.main')

@section('content')

{!! Form::open(['route'=>'backend.users.store', 'class'=> '']) !!}	

@include('backend.users.form')

{!! Form::close() !!}	


@endsection

@section('topscripts')
@endsection

@section('bottomscripts')
@endsection