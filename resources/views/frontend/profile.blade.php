@extends('frontend.layouts.main')

@section('content')

<div class="container">
	
	<!-- Page Heading/Breadcrumbs -->
	<div class="row">
	    <div class="col-lg-12">
	        <!-- <h1 class="page-header">Profil de <span class="strong">{{ $user->name }}</span></h1> -->
	    </div>
	</div>
	<!-- /.row -->
	
	<!-- Content Row -->
	<div class="row">
	    <!-- Sidebar Column -->
	    <div class="col-md-3">
	    
	        <h3>Options</h3>
	        <ul class="list-group">
	         <li class="list-group-item"><a class="titreEvent" href="#">Personal datas</li>
	        </ul>
	    </div>
	    <!-- Content Column -->
	    <div class="col-md-9">
	        <h2>{{ $user->name }}</h2>
	        
	        <ul class="list-group">
	        	<li class="list-group-item"><span class="strong">Name : </span>{{ $user->name }}</li>
	        	<li class="list-group-item"><span class="strong">Email : </span>{{ $user->email }}</li>
	        	<li class="list-group-item"></li>
	        </ul>
	        <p></p>
	    </div>
	</div>
	<!-- /.row -->
	
	<hr>

<!-- Footer -->
@include('frontend.partials.footer')


@endsection