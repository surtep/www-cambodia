@extends('frontend.layouts.main')

@section('content')
<div class="jumbotron jumbotronError">
<h1>Ups! </h1>
<h2>Houston, on a un problème !!</h2>
<p>On dirait qu'il n'y pas ou pas assez de EVENTS pour aujourd'hui.</p>
<p>Il faut être sur que : </p>
<ul>
	<li>il y a au moins un EVENT par catégorie pour aujourd'hui</li>
	<li>que cet EVENT soit 'aprouved'. Sinon il n'apparaît pas.</li>
</ul>
<p>Il faut vérifier cela dans le backend</p>
</div>
@endsection