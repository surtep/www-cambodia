<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
     <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900' rel='stylesheet' type='text/css'>

    <title>WWW-Cambodia</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/frontend/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/frontend/css/modern-business.css" rel="stylesheet">

    <!-- www-cambodia CSS -->
    <link href="/assets/frontend/css/www-cambodia.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/frontend/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
@inject('placeIcone', 'App\Services\PlaceIcone')

    <!-- Navigation -->
   
   
   @include('frontend.partials.navigation')

   {{-- @include('frontend.partials.carrousel') --}}

@yield('bigPicture')


    <!-- Page Content -->
    <div class="container">

    @yield('content')

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="/assets/frontend/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/frontend/js/bootstrap.min.js"></script>

</body>

</html>
