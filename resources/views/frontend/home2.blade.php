@extends('frontend.layouts.main')


@section('bigPicture')
<div class="row">
<div class="col col-lg-12 bigPicture">
            <div class="container">
                <div class="winnerAdd">
                <div class="winnerPicture">
                    <img src="{{ $topFiveAdds[0]->pictureEvent }}" alt="">
                </div>
                <div class="blockWinnerAdd">
                <div class="titleWinnerAdd strokeme">
                    {{ $topFiveAdds[0]->title }}
                </div>
                <div class="dateWinnerAdd strokeme">
                    {{ $topFiveAdds[0]->eventWhen }}
                </div>
                </div>
                </div>
            </div>
</div>
</div>
@endsection

@section('content')
        <!-- Marketing Icons Section -->
        

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">
                    Hot for today
                </h1>
            </div>
        
            <div class="col-md-4">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ $topFiveAdds[0]->pictureEvent }}')">
                    <h3>{{ $topFiveAdds[0]->title }}</h3>
                        <h4 class="whereTransparent{{ $topFiveAdds[0]->what->id }}">
                         {{ $topFiveAdds[0]->eventWhere }}
                         </h4>
                    </div>
                    <div class="panel-body bodyTopFiveAdd">
                        <p>{{ str_limit($topFiveAdds[0]->description, $limit = 150, $end = '...') }}</p>
                        <div class="topFiveBottom">
                            <div class=" what{{ $topFiveAdds[0]->what->id }} topFiveBottomLine">
                            </div>
                                <div class="bottomLogoPosition">
                                    <span><i class="fa fa-fw {{ $topFiveAdds[0]->what->icon }} what{{ $topFiveAdds[0]->what->id }} bottomLogo"></i></span>
                                </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>

<div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ $topFiveAdds[1]->pictureEvent }}')">
                    <h3>{{ $topFiveAdds[1]->title }}</h3>
                        <h4 class="whereTransparent{{ $topFiveAdds[1]->what->id }}">
                         {{ $topFiveAdds[1]->eventWhere }}
                         </h4>
                    </div>
                    <div class="panel-body bodyTopFiveAdd">
                        <p>{{ str_limit($topFiveAdds[1]->description, $limit = 150, $end = '...') }}</p>
                        <div class="topFiveBottom">
                            <div class=" what{{ $topFiveAdds[1]->what->id }} topFiveBottomLine">
                            </div>
                                <div class="bottomLogoPosition">
                                    <span><i class="fa fa-fw {{ $topFiveAdds[1]->what->icon }} what{{ $topFiveAdds[1]->what->id }} bottomLogo"></i></span>
                                </div>
                        </div>
                    </div>
                </div>
 </div>

 <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ $topFiveAdds[2]->pictureEvent }}')">
                    <h3>{{ $topFiveAdds[2]->title }}</h3>
                        <h4 class="whereTransparent{{ $topFiveAdds[2]->what->id }}">
                         {{ $topFiveAdds[2]->eventWhere }}
                         </h4>
                    </div>
                    <div class="panel-body bodyTopFiveAdd">
                        <p>{{ str_limit($topFiveAdds[2]->description, $limit = 150, $end = '...') }}</p>
                        <div class="topFiveBottom">
                            <div class=" what{{ $topFiveAdds[2]->what->id }} topFiveBottomLine">
                            </div>
                                <div class="bottomLogoPosition">
                                    <span><i class="fa fa-fw {{ $topFiveAdds[2]->what->icon }} what{{ $topFiveAdds[2]->what->id }} bottomLogo"></i></span>
                                </div>
                        </div>
                    </div>
                </div>
 </div>

         
        </div>

<div class="row">
    <div class="col-lg-4 col-lg-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ $topFiveAdds[3]->pictureEvent }}')">
                    <h3>{{ $topFiveAdds[3]->title }}</h3>
                        <h4 class="whereTransparent{{ $topFiveAdds[3]->what->id }}">
                         {{ $topFiveAdds[3]->eventWhere }}
                         </h4>
                    </div>
                    <div class="panel-body bodyTopFiveAdd">
                        <p>{{ str_limit($topFiveAdds[3]->description, $limit = 150, $end = '...') }}</p>
                        <div class="topFiveBottom">
                            <div class=" what{{ $topFiveAdds[3]->what->id }} topFiveBottomLine">
                            </div>
                                <div class="bottomLogoPosition">
                                    <span><i class="fa fa-fw {{ $topFiveAdds[3]->what->icon }} what{{ $topFiveAdds[3]->what->id }} bottomLogo"></i></span>
                                </div>
                        </div>
                    </div>
                </div>
 </div>

 <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ $topFiveAdds[4]->pictureEvent }}')">
                    <h3>{{ $topFiveAdds[4]->title }}</h3>
                        <h4 class="whereTransparent{{ $topFiveAdds[4]->what->id }}">
                         {{ $topFiveAdds[4]->eventWhere }}
                         </h4>
                    </div>
                    <div class="panel-body bodyTopFiveAdd">
                        <p>{{ str_limit($topFiveAdds[4]->description, $limit = 150, $end = '...') }}</p>
                        <div class="topFiveBottom">
                            <div class=" what{{ $topFiveAdds[4]->what->id }} topFiveBottomLine">
                            </div>
                                <div class="bottomLogoPosition">
                                    <span><i class="fa fa-fw {{ $topFiveAdds[4]->what->icon }} what{{ $topFiveAdds[4]->what->id }} bottomLogo"></i></span>
                                </div>
                        </div>
                    </div>
                </div>
 </div>
</div>


<div class="row">

<div class="col-lg-12">
                <h1 class="page-header">
                    Choix des internautes
                </h1>
            </div>
            
            <div class="col-lg-9 col-lg-offset-3">
                @foreach ($topFiveAdds as $topFiveAdd)
                <div class="panel panel-default ">
                    <div class="panel-body">
                    <div class="domaineTopFive">
                        <span><i class="fa fa-fw {{ $topFiveAdd->what->icon }} what{{ $topFiveAdd->what->id }}"></i></span>
                    </div>
                    <a href="{{ route('voirEvent', $topFiveAdd->slug) }}"><span class="titreEvent"></span>
                    <div class="vignette">
                    <img src="{{ $topFiveAdd->pictureEvent }}" alt="" >
                    </div>
                    <div class="titleTopFive">
                    {{ $topFiveAdd->title }}
                    </div>
                    <div class="textTopFive">
                    {{ str_limit($topFiveAdd->description, $limit = 150, $end = '...') }}
                    </div>
                    <div class="infoTopFive">
                    Where:<span class="labelInfoTopFive"> {{ $topFiveAdd->eventWhere }} </span>
                    When:  <span class="labelInfoTopFive">{{ $topFiveAdd->eventWhen }}</span>
                    What:  <span class="labelInfoTopFive">{{ $topFiveAdd->what_id }}</span>
                    </div>
                    </a>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
        <!-- /.row -->

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Portfolio Heading</h2>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Modern Business Features</h2>
            </div>
            <div class="col-md-6">
                <p>The Modern Business template by Start Bootstrap includes:</p>
                <ul>
                    <li><strong>Bootstrap v3.2.0</strong>
                    </li>
                    <li>jQuery v1.11.0</li>
                    <li>Font Awesome v4.1.0</li>
                    <li>Working PHP contact form with validation</li>
                    <li>Unstyled page elements for easy customization</li>
                    <li>17 HTML pages</li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="http://placehold.it/700x450" alt="">
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->
        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="#">Call to Action</a>
                </div>
            </div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>
@endsection