@extends('frontend.layouts.main')
@inject('placeCategorieliste', 'app\Services\PlaceCategorieListe')
@section('content')

<div class="container">

    <!-- Marketing Icons Section -->
    <div class="row">
        <div class="col col-lg-6">
            <img src="assets/frontend/img/logowww-home.png" alt="">
        </div>
        <div class="col-lg-6">
            <h1 class="page-header">
                Welcome to www-Cambodia
            </h1>
                <p class="introHome">www-Cambodia is the place to find the good information at the good moment. <span class="text-primary">Who</span> makes an Event ? <span class="text-primary">When</span> is this event? <span class="text-primary">Where</span> is the event? www-Cambodia gives you the answer and you just have to enjoy it.</p>
                <p class="subIntroHome">Autres explications commerciales ( pour Mr O ).</p>
                <p class="subIntroHome">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, quisquam, dignissimos, magni, ea accusamus provident illo corporis non cumque officia esse qui rerum quae quidem at fugit culpa aliquid delectus.</p>
                <a href="https://www.facebook.com/pages/WWW-Cambodia/1536685173257264" class="pull-right"><img src="/assets/frontend/img/facebook.jpg" alt="Facebook" style="width:130px"></a>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12">
    <hr>
            <h2 style="display:flex">Choose an event in<span class="strong"><ul style="margin-left:-33px; margin-right:8px">
<li class="dropdown" style="list-style-type:none">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if(session()->has('cityActive'))
                {{ session('cityActive') }}
                @else
                Phnom Penh
                @endif

            <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ route('setCity', 'Phnom Penh') }}">Phnom Penh</a>
                </li>
                <li>
                    <a href="{{ route('setCity', 'Siem Reap') }}">Siem Reap</a>
                </li>
                <li>
                    <a href="{{ route('setCity', 'Sihanoukville') }}">Sihanoukville</a>
                </li>
            </ul>
        </li>
                
            </ul>

            </span>and ... GO!</h2>
    <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><i class="fa fa-fw fa-calendar"></i> Daily / Week</h4>
                </div>
                <div class="panel-body">
               
               
                <ul class="list-group">
                @if(count($eventsDWs))
                    @foreach ($eventsDWs as $event)
                    <li class="list-group-item"><span class="badge {{ $event->categorie->title }}">{{ $event->categorie->title }}</span> <a href="{{ route('voirEvent', $event->slug) }}"><span class="titreEvent">{{ $event->title }}</span> <br> <span class="diffHumans">{{ $event->when->diffForHumans() }}</span>  <span class="diffSeconds">{{ $event->when->diffInSeconds() }}</span></a></li>
                    @endforeach
                    @else
                    <li class="list-group-item">{{ trans('home.noEventsCategory') }}</li>
                    @endif
                </ul>
                </div>
                <div class="panel-footer">
                    <h6><i class="fa fa-fw fa-plus-square"></i> {!! trans('home.eventsForTomorrow', ['combienEventTomorrow' => $placeCategorieliste->combienEventDemain(['Daily', 'Week'])]) !!}</h6>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><i class="fa fa-fw fa-cutlery"></i> Resto / Music</h4>
                </div>
                <div class="panel-body">
              
                    <ul class="list-group">
                                        @if(count($eventsRMs))
                                        @foreach ($eventsRMs as $event)
                                        <li class="list-group-item"><span class="badge {{ $event->categorie->title }}">{{ $event->categorie->title }}</span> <a href="{{ route('voirEvent', $event->slug) }}"><span class="titreEvent">{{ $event->title }}</span> <br> <span class="diffHumans">{{ $event->when->diffForHumans() }}</span>  <span class="diffSeconds">{{ $event->when->diffInSeconds() }}</span></a></li>
                                        @endforeach
                                        @else
                                        <li class="list-group-item">{{ trans('home.noEventsCategory') }}</li>
                                        @endif
                                    </ul>
                </div>
                        <div class="panel-footer">
                            <h6><i class="fa fa-fw fa-plus-square"></i> {!! trans('home.eventsForTomorrow', ['combienEventTomorrow' => $placeCategorieliste->combienEventDemain(['Resto', 'Music'])]) !!}</h6>
                        </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><i class="fa fa-fw fa-newspaper-o"></i> News</h4>
                </div>
                <div class="panel-body">
               
                    <ul class="list-group">
                                        @if (count($eventsNs))
                                        @foreach ($eventsNs as $event)
                                        <li class="list-group-item hover">
                                        <span class="badge {{ $event->categorie->title }}">{{ $event->categorie->title }}</span><a href="{{ route('voirEvent', $event->slug) }}"><span class="titreEvent">{{ $event->title }}</span> <br> <span class="diffHumans">{{ $event->when->diffForHumans() }}</span>  <span class="diffSeconds">{{ $event->when->diffInSeconds() }}</span></a>
                                        </li>
                                        @endforeach
                                        @else
                                        <li class="list-group-item">{{ trans('home.noEventsCategory') }}</li>
                                        @endif
                                    </ul>
                </div>
                <div class="panel-footer">
                    <h6><i class="fa fa-fw fa-plus-square"></i> {!! trans('home.eventsForTomorrow', ['combienEventTomorrow' => $placeCategorieliste->combienEventDemain(['News'])]) !!}</h6>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <hr>

    {{-- @include('frontend.partials.addEventUser'); --}}


    <!-- Footer -->
    @include('frontend.partials.footer')

</div>


@endsection

@section('topscripts')
<link rel="stylesheet" href="{{ asset('assets/backend/css/jquery.simple-dtpicker.css') }}">
@endsection


@section('bottomscripts')
<script>
$(function () {
	// $('.catyBadge').each(function(){
	// 	$(this).addClass("badge " + $(this).html());
	// });

    $('.diffSeconds').each(function(){
        if($(this).html()<120*60){
            // $(this).siblings('.diffHumans').html(compteRebours($(this).html()));
            // $(this).siblings('.diffHumans').html();
            startTimer($(this).html(), $(this).siblings('.diffHumans'))
        }
    });

    function compteRebours($minutes){
        return 'il reste ' + $minutes * 60 + ' secondes ';
    };

    function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        if(minutes==00){
        display.text('dans '  + seconds + ' secondes');
        } else {
        display.text('dans ' +minutes + ' minutes et ' + seconds + ' secondes');
    }

        if (--timer < 0) {
           $(display).parent().parent().remove();
           return display.text('Cet Event a déjà commencé');

        }
    }, 1000);
}


});
</script>

<script type="text/javascript" src="{{ asset('assets/backend/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/backend/ckeditor/adapters/jquery.js') }}"></script>
<script src="/assets/backend/ckeditor/maConfig.js"></script>

<script src="{{ asset('assets/backend/js/jquery.simple-dtpicker.js') }}"></script>
<script type="text/javascript">
    $(function(){
        $('*[name=when]').appendDtpicker({'closeOnSelected':true});
        $('*[name=published_at]').appendDtpicker({'closeOnSelected':true});
    });
</script>

@endsection