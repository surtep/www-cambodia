@inject('placeWhatListe', 'App\Services\PlaceWhatListe')

@extends('frontend.layouts.main')

@section('content')

<div class="container">
	
	<!-- Page Heading/Breadcrumbs -->
	<div class="row">
	    <div class="col-lg-12">
	        
	    </div>
	</div>
	<!-- /.row -->
	<?php 
                    $titleLoc = "title_".Session::get('applocale');
                    $titleLoc = "title_en";
                    $descriptionLoc = "description_".Session::get('applocale');
                    $descriptionLoc = "description_en";
                 ?>
	<!-- Content Row -->
	<div class="row">
	    <!-- Sidebar Column -->
	    <div class="col-md-3">
	    <h3>Daily</h3>
	        <ul class="list-group">
	        @if(count($placeWhatListe->listeWhat('Daily')))
	            @foreach ($placeWhatListe->listeWhat('Daily') as $eventMenu)
	            	<li class="list-group-item"><a class="titreEvent" href="{{ route('voirEvent', $eventMenu->slug) }}">{{ str_limit($eventMenu->$titleLoc, 25) }}</a><br><span>{{ $eventMenu->when->diffForHumans() }}</span></li>
	            @endforeach
	        @else
	        <li class="list-group-item danger">Sorry, No Items in this category</li>
	        @endif
	        </ul>
	        <h3>Week</h3>
	        <ul class="list-group">
	        @if(count($placeWhatListe->listeWhat('Week')))
	            @foreach ($placeWhatListe->listeWhat('Week') as $eventMenu)
	            	<li class="list-group-item"><a class="titreEvent" href="{{ route('voirEvent', $eventMenu->slug) }}">{{ str_limit($eventMenu->$titleLoc, 25) }}</a><br><span>{{ $eventMenu->when->diffForHumans() }}</span></li>
	            @endforeach
        	        @else
	        <li class="list-group-item danger">Sorry, No Items in this category</li>
	        @endif
	        </ul>
	        <h3>Resto</h3>
	        <ul class="list-group">
		@if(count($placeWhatListe->listeWhat('Resto')))
	            @foreach ($placeWhatListe->listeWhat('Resto') as $eventMenu)
	            	<li class="list-group-item"><a class="titreEvent" href="{{ route('voirEvent', $eventMenu->slug) }}">{{ str_limit($eventMenu->$titleLoc, 25) }}</a><br><span>{{ $eventMenu->when->diffForHumans() }}</span></li>
	            @endforeach
	            @else
	            <li class="list-group-item danger">Sorry, No Items in this category</li>
	            @endif
	        </ul>
	        <h3>Music</h3>
	        <ul class="list-group">
		@if(count($placeWhatListe->listeWhat('Music')))
	            @foreach ($placeWhatListe->listeWhat('Music') as $eventMenu)
	            	<li class="list-group-item"><a class="titreEvent" href="{{ route('voirEvent', $eventMenu->slug) }}">{{ str_limit($eventMenu->$titleLoc, 25) }}</a><br><span>{{ $eventMenu->when->diffForHumans() }}</span></li>
	            @endforeach
	            @else
	            <li class="list-group-item danger">Sorry, No Items in this category</li>
	            @endif
	        </ul>
	        <h3>News</h3>
	        <ul class="list-group">
	        @if(count($placeWhatListe->listeWhat('News')))
	            @foreach ($placeWhatListe->listeWhat('News') as $eventMenu)
	            	<li class="list-group-item"><a class="titreEvent" href="{{ route('voirEvent', $eventMenu->slug) }}">{{ str_limit($eventMenu->$titleLoc, 25) }}</a><br><span>{{ $eventMenu->when->diffForHumans() }}</span></li>
	            @endforeach
	            @else
	            <li class="list-group-item danger">Sorry, No Items in this category</li>
	            @endif
	        </ul>
	    </div>
	    <!-- Content Column -->
	    <div class="col-md-9">
	        <h2>{{ $event->$titleLoc }}</h2>
	        <img src="{{ $event->pictureEvent }}" alt="" width="350">
	        {{-- {{dd($event->when->diffInMinutes()) }} --}}
		@if( $event->when->diffInMinutes() < '60')
			<div class="alert alert-dismissible alert-danger">
			  <button type="button" class="close" data-dismiss="alert">×</button>
			  {!! trans('events.eventsInLess60') !!}
			</div>
		@endif
	        <ul class="list-group">
	        	<li class="list-group-item"><span class="strong">When : </span> {{ $event->when->toDayDateTimeString() }} - {{ $event->when->diffForHumans() }}</li>
	        	<li class="list-group-item"><span class="strong">Where : </span> {{ $event->where }}</li>
	        	<li class="list-group-item"></li>
	        </ul>
	        <p>{{ $event->$descriptionLoc }}</p>
	    </div>
	</div>
	<!-- /.row -->
	
	<hr>

<!-- Footer -->
@include('frontend.partials.footer')

@endsection