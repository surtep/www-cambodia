@inject('categoriesSelect', 'App\Services\CreateSelectCategories')

{!! Form::open(['route'=>'storeEvent', 'class'=> '']) !!}	

<div class="row">
	<div class="col-md-12 col-sm-12">
				<div class="row">
					<div class="col col-lg-8">
						<div class="form-group">
						    {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
						    {!! Form::text('title', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-md-2">
	                        	                         <span class="form-group pull-right">
	                        	                         
	                        	                             {!! Form::label('categorie_id', 'Category : ', ['class' => 'control-label']) !!}
	                        	                             {!! Form::select('categorie_id', App\Categorie::lists('title', 'id'), null, ['class' => 'form-control'] ) !!}
	                        	                         </span>
		        	                        </div>
					<div class="col col-md-2">
	                        	                         <span class="form-group pull-right">
         	                        	                             {!! Form::label('city', 'City : ', ['class' => 'control-label']) !!}
	                        	                             {!! Form::select('city', App\City::lists('city', 'id'), null, ['class' => 'form-control'] ) !!}
	                        	                         </span>
		        	                        </div>
					
				</div>
				<div class="row">
					<div class="col col-md-6">
						<div class="form-group">
						    {!! Form::label('where', 'Where:', ['class' => 'control-label']) !!}
						    {!! Form::text('where', null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="col col-md-3">
						{!! Form::label('when', 'When', ['class' => 'control-label']) !!}
						{!! Form::text('when', null, ['class' => 'form-control']) !!}
					</div>
					<div class="col col-md-3">
						{!! Form::label('published_at', 'Publish date', ['class' => 'control-label']) !!}
						{!! Form::text('published_at', null, ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
						    {!! Form::label('content', 'Description:', ['class' => 'control-label']) !!}
						    {!! Form::textarea('content', null, ['class' => 'form-control ckeditor']) !!}
						</div>
					</div>
					
				</div>
				<div class="btn-group" role="group">
					{!! Form::submit('Send', ['class' => "btn btn-primary btn-sm btn-savepage"]) !!}
				</div>

{!! Form::close() !!}	                                
