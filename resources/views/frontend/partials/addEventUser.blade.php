<div class="row">
    <div class="col col-lg12">
        <h2>Want to publish an event here?</h2>
        <p>Are you planning a social event in PP, SRP, or Sianoukville? Don't hesitate and promote here. It's free. Just fill in the form and after supervision your event will be published.</p>
        @if(Auth::user())
            @include('frontend.partials.eventform')
        @else
            @include('frontend.partials.loginform')
        @endif
    </div>
</div>