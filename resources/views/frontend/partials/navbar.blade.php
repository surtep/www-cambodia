<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        @include('frontend.partials.prenavbar')
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/assets/frontend/img/logowww70.png" alt="Home" style="margin-top: -25px;"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        @include('frontend.partials.menusuperieur')
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>