<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;

class SeedUsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
        [
        'name' => 'Pierre',
        'email' => 'pjourdain@gmail.com',
        'phone' => '+32484383210',
        'password' => bcrypt('123456'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ],
        [
        'name' => 'Iascha',
        'email' => 'iascha@hotmail.com',
        'phone' => '',
        'password' => bcrypt('123456'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ]
        ];

        DB::table('users')->insert($users);
                    
    }
}
