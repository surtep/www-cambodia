<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
        [
        'name'=>'SuperAdmin', 
        'slug'=>'superadmin', 
        'description' => 'la description de Super Admin', 
        'level' => 1
        ],
        [
        'name'=>'Admin', 
        'slug'=>'admin', 
        'description' => 'la description de CA', 
        'level' => 2
        ],
        [
        'name'=>'Redacteur', 
        'slug'=>'redacteur', 
        'description' => 'la description de membre', 
        'level' => 3
        ],
        [
        'name'=>'user', 
        'slug'=>'user', 
        'description' => 'la description de public', 
        'level' => 4
        ]
        ];

        DB::table('roles')->insert($roles);
    }
}
