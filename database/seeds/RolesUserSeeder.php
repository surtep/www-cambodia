<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class RolesUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('role_user')->delete();

        $role_user = [
        [
        'role_id' =>1,
        'user_id' =>1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ],
        [
        'role_id' =>1,
        'user_id' =>2,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ]
        ];

        DB::table('role_user')->insert($role_user);
                    

    }
}
