<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('cities')->truncate();

        $cities = [
        [
        'city'=>'Phnom Penh',
         'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ],
        [
        'city'=>'Siem Reap',
         'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ],
        [
        'city'=>'Sihanoukville',
         'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ]
        ];
       DB::table('cities')->insert($cities);
    }
}
