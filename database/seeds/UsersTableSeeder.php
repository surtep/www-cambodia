<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();


        $users = [
        [
        'name' => 'Pierre',
        'idcrypt' => 'fdftretre',
        'email' => 'pjourdain@gmail.com',
        'password' => bcrypt('123456'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ],
        [
        'name' => 'Iascha',
        'idcrypt' => 'mlklkmlk',
        'email' => 'iascha@hotmail.com',
        'password' => bcrypt('123456'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        ]
        ];

        DB::table('users')->insert($users);
                    
    }
}
