<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Event::class, function ($faker) {
  return [
    'title' => $faker->sentence(mt_rand(3, 10)),
    'content' => join("\n\n", $faker->paragraphs(mt_rand(3, 6))),
    'where' => $faker->sentence(mt_rand(3, 25)),
    'when' => $faker->dateTimeBetween('-1 days', '+15 days'),
    'created_by' => 1,
    'city_id' => $faker->numberBetween($min = 1, $max = 3),
    'categorie_id' => $faker->numberBetween($min = 1, $max = 5),
    'published_at' => $faker->dateTimeBetween('-1 days', '+15 days'),
    'created_at' => $faker->dateTimeBetween('-1 days', '+3 days'),
  ];
});

// $factory->define(App\User::class, function ($faker) {
//     return [
//         'name' => $faker->name,
//         'email' => $faker->email,
//         'password' => str_random(10),
//         'remember_token' => str_random(10),
//     ];
// });
