<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('events', function($table) {
              $table->increments('id');
              $table->string('slug')->unique();
              $table->integer('what_id');
              $table->string('title_en');
              $table->string('title_kh');
              $table->string('title_fr');
              $table->text('description_en');
              $table->text('description_kh');
              $table->text('description_fr');
              $table->string('pictureEvent');
              $table->string('eventWho');
              $table->string('eventWhere');
              $table->timestamp('eventWhen');
              $table->time('when_end');
              $table->integer('position')->unsigned()->default(0);
              $table->integer('likes')->unsigned()->default(0);
              $table->integer('created_by')->unsigned();
              $table->integer('city_id')->unsigned();
              $table->integer('approved')->unsigned()->default(0);

              $table->timestamps();
              $table->timestamp('published_at')->index();
           });

        Schema::table('events', function($table) {
               $table->foreign('what_id')->references('id')->on('whats');
               $table->foreign('created_by')->references('id')->on('users');
               $table->foreign('city_id')->references('id')->on('cities');
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
