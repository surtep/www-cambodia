<?php

namespace App\Http\Middleware;

use Closure;

class InConstruction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(! \Auth::User())
        {
           return view('frontend.layouts.inconstruction');
        }
        return $next($request);
    }
}
