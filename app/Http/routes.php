<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// \Carbon\Carbon::setLocale('en');


if(isset($_COOKIE['Language'])){
	session(['languageActive' => $_COOKIE['Language']]);
} else {
	session(['languageActive' => 'English']);
	setcookie("Language", 'English', time()+3600*24*365, '/');
}

Route::group(['middleware'=>'inconstruction'], function(){

Route::get('/', ['as' => 'home', "uses"=> "FrontendController@home"]);

Route::get('/eve/{num}', ['as' => 'voirEvent', 'uses' => 'FrontendController@view']);
Route::get('api/setCity/{city}', ['as' => 'setCity', 'uses' => 'FrontendController@setCity']);
// Route::get('api/setLanguage/{language}', ['as' => 'setLanguage', 'uses' => 'FrontendController@setLanguage']);
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
Route::post('api/storeEvent/{language}', ['as' => 'storeEvent', 'uses' => 'FrontendController@storeEvent']);
Route::get('events/cat/{id}', ['as'=>'pageCat', 'uses'=>'FrontendController@pageView']);

Route::get('profile/{email}', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
});

Route::get('login', ['as'=>'login', 'uses' => function(){
	return view('auth.login');
}]);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('login/{provider?}', 'Auth\AuthController@login');

Route::group(['prefix' => 'backend', 'middleware'=>'auth'], function () {
	Route::get('/', function(){
		return view('backend.home');
	});

Route::get('api/enregistrerChangeEvent', ['as' => 'enregistrerChangeEvent', 'uses' => 'BackendController@enregistrerChangeEvent']);

Route::get('api/enregistrerChangeEnd', ['as' => 'enregistrerChangeEnd', 'uses' => 'BackendController@enregistrerChangeEnd']);

	Route::resource('events', 'EventController');
	Route::resource('tomoderate', 'ModerateController@index');
	Route::resource('whats', 'WhatController');
	Route::resource('users', 'UserController');
	Route::get('changePassword', ['as' => 'changePassword', 'uses' => 'UserController@changePassword']);
	Route::post('updatePassword', ['as' => 'updatePassword', 'uses' => 'UserController@updatePassword']);

	Route::get('filemanager', function(){
		return view('backend.filemanager.index');
	});
	Route::get('calendar', function(){ return view('backend.events.calendar');});
	Route::get('tests', 'TestsController@index');
});