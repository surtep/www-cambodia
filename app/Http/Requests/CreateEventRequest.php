<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|min:5',
            'description_en' => 'required',
            'eventWhere' => 'required',
            'eventWhen' => 'required',
            'when_end' => 'required',
            'pictureEvent' => 'required'
        ];
    }

    private function endValide(){
        return true;
    }
}
