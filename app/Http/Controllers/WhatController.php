<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\What;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateWhatRequest;

class WhatController extends Controller
{
    public function index()
    {
        $whats = What::all();
        return view('backend.whats.index', compact('whats'));
    }

    public function create()
    {
        return view('backend.whats.create');
    }

    public function store(CreateWhatRequest $request)
    {
        $what = What::create($request->all());
        return redirect()->route('backend.whats.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $what = What::findOrFail($id);
        return view('backend.whats.edit', compact('what'));
    }

    public function update(Request $request, $id)
    {
        $what = What::findOrFail($id);
        $what->update($request->all());

        return redirect()->route('backend.whats.index');
      
    }

    public function destroy($id)
    {
        $what = What::findOrFail($id);
        $what->delete();
        
        return redirect()->route('backend.whats.index');
    }
}
