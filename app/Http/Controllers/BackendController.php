<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;

class BackendController extends Controller
{
    public function enregistrerChangeEvent(Request $request)
    {
	$pieces = explode("T", $request['newHeure']);
	$when_end = $pieces[1];

	$pieces = explode("T", $request['newDate']);
	$nouvDate = $pieces[0] . ' ' .$pieces[1];

	$event = Event::findOrFail($request['id']);
	$event->eventWhen = $nouvDate;
	$event->when_end = $when_end;
	$event->save();
    }

public function enregistrerChangeEnd(Request $request)
    {
	$pieces = explode("T", $request['newHeure']);
	$when_end = $pieces[1];

	$event = Event::findOrFail($request['id']);
	$event->when_end = $when_end;
	$event->save();
    }


}
