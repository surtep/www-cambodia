<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;

class ModerateController extends Controller
{
    public function index()
    {
    	$tomoderates = Event::where('approved', '=', '0')->get();
    	return view('backend.moderate.index', compact('tomoderates'));
    }
}
