<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\What;
use App\Event;
use DB;

class TestsController extends Controller
{
    public function index()
    {
    	// $whats = What::all($title, 'id', 'icon');
    	$lang = \Config::get('app.locale');
    	$events = Event::get([
    	                     DB::raw('*'),
    	                     DB::raw('IF(title_'.$lang.'="", title_en, title_'.$lang.') as title'), 
    	                     DB::raw('IF(description_'.$lang.'="", description_en, description_'.$lang.') as description')
    	                     ]);

    	// dd($events);
	return view('backend.tests.index', compact('events'));
    }
}
