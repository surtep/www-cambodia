<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   protected $table = 'cities';
   protected $fillable = ['city'];

   public function event()
        {
            return $this->hasMany('App\Event');
        }
   
}
