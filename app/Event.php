<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;
use App\What;
use DB;


class Event extends Model
{
    protected $dates = ['published_at', 'eventWhen'];
    protected $fillable= ['slug', 'what_id', 'title_en', 'title_kh', 'title_fr', 'description_en', 'description_kh', 'description_fr', 'pictureEvent', 'position', 'likes', 'approved', 'published_at', 'eventWhere', 'eventWhen', 'created_by', 'city_id', 'when_end', 'eventWho'];

public function setTitleEnAttribute($value)
    {
    $this->attributes['title_en'] = $value;
            if (! $this->exists) {
        $this->attributes['slug'] = str_slug($value);
            }
    }

public function scopeTopFiveAdds($query)
{
$lang = \Config::get('app.locale');
    return $query->orWhere('city_id', '=', $this->trouveIdCity())
        ->where('approved', '=', 1)
        ->where('eventWhen', '>', Carbon::today())
        ->where('eventWhen', '<', Carbon::today()->addDay())
        ->orderBy('position', 'DESC')
       ->get([
                             DB::raw('*'),
                             DB::raw('IF(title_'.$lang.'="", title_en, title_'.$lang.') as title'), 
                             DB::raw('IF(description_'.$lang.'="", description_en, description_'.$lang.') as description')
                             ]);
}

    public function scopeAddNumberOne($query)
    {
$lang = \Config::get('app.locale');
        return $query->orWhere('city_id', '=', $this->trouveIdCity())
        ->where('approved', '=', 1)
        ->where('eventWhen', '>', Carbon::today())
        ->where('eventWhen', '<', Carbon::today()->addDay())
        ->orderBy('position', 'DESC')->first(
        [
                     DB::raw('*'),
                     DB::raw('IF(title_'.$lang.'="", title_en, title_'.$lang.') as title'), 
                     DB::raw('IF(description_'.$lang.'="", description_en, description_'.$lang.') as description')
         ]
        );
    }

    public function scopeEventtoday($query)
    {
        return $query->orWhere('city_id', '=', $this->trouveIdCity())
        ->where('eventWhen', '>', Carbon::today())
        ->where('eventWhen', '<', Carbon::today()->addDay())
        ->orderBy('position', 'DESC');
    }

    public function scopeEventtomorrow($query)
    {
        return $query->orWhere('city_id', '=', $this->trouveIdCity())
        ->where('eventWhen', '>', Carbon::today()->addDay())
        ->where('eventWhen', '<', Carbon::today()->addDay(2))
        ->orderBy('eventWhen', 'ASC');
    }

    public function scopeEventnext($query)
    {
        return $query->orWhere('city_id', '=', $this->trouveIdCity())
        ->where('eventWhen', '>', Carbon::today()->addDay(2))
        ->orderBy('eventWhen', 'ASC');
    }

    public function scopeEventdemain($query, $cat)
    {
        return $query->orWhere('what_id' , '=', $this->trouveIdWhat($cat))
        ->where('city_id', '=', $this->trouveIdCity())
        ->where('eventWhen', '>', Carbon::today()->addDay())
        ->where('eventWhen', '<', Carbon::today()->addDay(2));
    }

    public function scopeWhattitle($query, $cat)
       {
          return $query->orWhere('What_id' , '=', $this->trouveIdWhat($cat) )
            ->where('city_id', '=', $this->trouveIdCity())
            ->where('eventWhen', '>', Carbon::now() )
            ->where('eventWhen', '<', Carbon::today()->addDay())
            ->where('approved', '=', '1')
            ->orderBy('position', 'DESC')
            ->orderBy('eventWhen', 'ASC')
            ->limit(5);            
       }

private function trouveIdWhat($cattitle){
    $what = What::where('title_en', '=',  $cattitle)->first();
    $what_id = $what->id;
    return $what_id;
}

private function trouveIdCity(){
    session('cityActive') ? $laVille = session('cityActive') : $laVille = 'Phnom Penh';
    $city = City::where('city', '=',  $laVille)->first();
    $city ? $city_id = $city->id : $city_id = 00 ;
    return $city_id;
}

    public function what()
        {
            return $this->belongsTo('App\What');
        }

    public function user()
        {
            return $this->belongsTo('App\User');
        }
 public function city()
        {
            return $this->belongsTo('App\City');
        }

}
