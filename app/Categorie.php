<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
protected $fillable = ['title'];

    public function event()
        {
            return $this->hasMany('App\Event');
        }
}
