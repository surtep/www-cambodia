<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class What extends Model
{
protected $fillable = ['title_en', 'title_kh', 'title_fr', 'icon'];

    public function event()
        {
            return $this->hasMany('App\Event');
        }
}
