<?php
namespace App\Services;
use App\What;
use App\Event;
use Carbon\Carbon as Carbon;

class PlaceIcone
{
	public function icone($cat){

		$laCat = What::find($cat);
		$lesClasses = $laCat->icon ." what". $laCat->id;
		return $lesClasses;

	}

	public function makeListeWhats()
	{
		$lang = \Config::get('app.locale');
	    	$title = "title_".$lang." AS title";
		$whats = What::all($title, 'id', 'icon');
		return $whats;
	}
	
}