<?php
namespace App\Services;
use App\What;
use App\Event;
use Carbon\Carbon as Carbon;

class PlaceWhatListe
{
	public function listeWhat($cat){
		return Event::Whattitle($cat)->get();
	}

	public function combienEventDemain($cats)
	{
		$combien = 0;
		foreach ($cats as $cat) {
			$combien = $combien + Event::Eventdemain($cat)->count();
		}
		return $combien;
	}
}