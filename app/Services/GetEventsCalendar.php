<?php
namespace App\Services;
use App\Categorie;
use App\Event;
use Carbon\Carbon as Carbon;

class GetEventsCalendar
{
	public function getEventsList(){
		// $events = \DB::table('events')->select('id','title_en', 'when as start')->get();
		$events = Event::All();
		// dd($events);

		$tabEvent = [];
		foreach ($events as $event) {
			$elEvent = [];
			$title = $event->title_en;
			$date = substr($event->eventWhen, 0, 10);
			$start = substr($event->eventWhen, 11);
			$end = $event->when_end;
			$elEvent['id'] = $event->id;
			$elEvent['title'] = $title;
			$elEvent['start'] =  $date .'T'. $start;
			$elEvent['end'] =  $date .'T'. $end;
			$elEvent['color'] =  $this->selColor($event->what_id, $event->approved);
			$elEvent['url'] =  '/backend/events/'. $event->id.'/edit' ;
			// $elEvent['url'] =  'http://www.google.com';
			
			$tabEvent[] = $elEvent;
		};


		$tabEvent = json_encode($tabEvent);
		// dd($tabEvent);
		return $tabEvent;
	}


	private function selColor($what_id, $approved)
	{
		if($approved){
		$colores = config('www-cambodia.couleursDomaines');
		return $colores[$what_id];
		} else {
			return '#cccccc';
		}
	}

	// public function combienEventDemain($cats)
	// {
	// 	$combien = 0;
	// 	foreach ($cats as $cat) {
	// 		$combien = $combien + Event::Eventdemain($cat)->count();
	// 	}
	// 	return $combien;
	// }
}